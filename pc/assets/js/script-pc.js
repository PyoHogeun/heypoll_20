
$('document').ready(function(){
  $('.accordion-list > ul > li').click(function(e){
    $(this).toggleClass('on');
    $(this).find('.detail').slideToggle();
  });

  $('.btn-all-menu').click(function(e){
    e.preventDefault();
    $('.all-menu').toggleClass('open');
    $('nav.top-fixed').toggleClass('shadow').toggleClass('border-bottom');
    $('.all-menu.open .dimmer').click(function(){
      $('.all-menu').removeClass('open');
      $('nav.top-fixed').addClass('shadow').removeClass('border-bottom');
    });
    $('.menu-wrap .btn-close').click(function(e){
      e.preventDefault();
      $('.all-menu').removeClass('open');
    })
  });


  // 커스텀 스크롤
  $('.scrollbar-inner').scrollbar();

  //aside 검색 옵션 토글
  searchOptionToggle();
})

function mainBannerCounter(event){
  var element   = event.target;         // DOM element, in this example .owl-carousel
  var items     = event.item.count;     // Number of items
  var item      = event.item.index + 1;     // Position of the current item

  // it loop is true then reset counter from 1
  if(item > items) {
    item = item - items
  }
  $('#mainBannerCounter').html("<span class='now'>"+item+"</span><span class='partition'></span><span class='total'>"+items+"</span>")
}
function asideBannerCounter(event){
  var element   = event.target;         // DOM element, in this example .owl-carousel
  var items     = event.item.count;     // Number of items
  var item      = event.item.index + 1;     // Position of the current item

  // it loop is true then reset counter from 1
  if(item > items) {
    item = item - items
  }
  $('#asideBannerCounter').html("<span class='now'>"+item+"</span><span class='partition'></span><span class='total'>"+items+"</span>")
}

function searchOptionToggle(){
  $('.option-toggle').click(function(e){
    $(this).toggleClass('on');
    $('.option-list').slideToggle();
    $('.search-option').toggleClass('on');
  })
}