.main{
  .content-wrap{
    display: block;
    // flex-basis: calc(100% - #{$aside-width} - #{$content-aside-gap});
    .header{
      @include flex-prefix-suffix;
      .title{
        display: inline-block;
        font-size: rem(22);
        font-weight: $bold;
        color: $fc-default;
        $scale: 0.50;
        [class*="text-img-"]{
          display: inline-block;
          background-repeat: no-repeat;
          background-size: contain;
          height: rem(34*$scale);
        }
        .text-img-play{
          background-image: url(../images/text_img_play.png);
          width: rem(102*$scale);
        }
        .text-img-pick{
          background-image: url(../images/text_img_pick.png);
          width: rem(90*$scale);
        }
        .text-img-mall{
          background-image: url(../images/text_img_mall.png);
          height: rem(16);
          width: rem(41);
          vertical-align: middle;
        }
        .sub-text{
          font-size: rem(14);
          font-weight: $normal;
          color: $fc-gray;
          margin-left: rem(30);
        }
      }
      .slide-paging{
        position: absolute;
        display: flex;
        justify-content: center;
        align-items: center;
        width: rem(90);
        height: rem(50);
        color: lighten($fc-default, 70%);
        font-size: rem(28);
        z-index: 1;
        .now{
          color: $fc-default;
          font-weight: $bold;
        }
      }
      .btn-round{
        font-size: rem(13);
        line-height: 1.4;
      }
      .btn-white{
        padding-left: rem(30);
        padding-right: rem(15);
        box-shadow: $thin-shadow;
      }
    }
    .template-bg-gray{
      // width: calc(100% - #{$container-side-pad} * 2);
      padding-left: $container-side-pad;
      padding-right: $container-side-pad;
      padding-top: rem(10);
      // padding-bottom: $container-side-pad;
      background-color: $bg-gray;
      // border-radius: rem(10);
    }
    .main-banner{
      position: relative;
      width: calc(#{$container-width} - #{$aside-width} - #{$content-aside-gap} - #{$container-side-pad} - #{$container-side-pad});
      box-shadow: $main-banner-shadow;
      .owl-carousel{
        height: rem(300);
      }
      .banner-link{
        display: inline-block;
        
      }
      .custom-nav{
        position: absolute;
        bottom: rem(-10);
        right: rem(60);
        display: flex;
        width: rem(95);
        height: rem(25);
        border-radius: rem(30);
        background-color: rgba(121,61,255,.8);
        box-shadow: $template-shadow;
        z-index: 1;
        .owl-next, .owl-prev{
          position: absolute;
          display: inline-block;
          width: rem(8);
          top: rem(5);
          // transform: translateY(-50%);
        }
        .owl-prev{
          left: rem(10);
        }
        .owl-next{
          right: rem(10);
        }
      }
      .slide-paging{
        position: absolute;
        display: flex;
        justify-content: center;
        align-items: center;
        bottom: rem(-10);
        right: rem(85);
        width: rem(45);
        height: rem(25);
        color: rgba(255,255,255,0.5);
        font-size: rem(12);
        z-index: 1;
        .partition{
          display: inline-block;
          width: rem(1);
          height: rem(10);
          border-right: rem(1) solid rgba(255,255,255,0.5);
          margin: 0 rem(10);
        }
        .now{
          color: #ffffff;
          font-weight: $bold;
        }
      }
    }
    .today-play{
      width: calc(#{$container-width} - #{$aside-width} - #{$content-aside-gap} - #{$container-side-pad} * 4);
      padding-bottom: rem(35);
      .owl-carousel{
        $multi: 0.55;
        .icon-slide-prev{
          width: rem(8*$multi);
          height: rem(12*$multi);
          background-size: rem(750*$multi);
          background-position: rem(-326*$multi) rem(-2265*$multi);
        }
        .icon-slide-next{
          width: rem(8*$multi);
          height: rem(12*$multi);
          background-size: rem(750*$multi);
          background-position: rem(-326*$multi) rem(-2278*$multi);
        }
        .owl-nav{
          position: absolute;
          top: rem(-40);
          right: 0;
          display: flex;
          align-items: center;
          width: rem(60);
          height: rem(25);
          &:after{
            content: '';
            display: inline-block;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            height: rem(12);
            width: rem(1);
            border-right: $border-gray;
          }
          .owl-next, .owl-prev{
            position: absolute;
            display: flex;
            align-items: center;
            width: rem(8*$multi);
            height: rem(12*$multi);
            top: rem(9);
          }
          .owl-prev{
            left: rem(5);
          }
          .owl-next{
            right: rem(5);
          }
        }
        .item{
          display: block;
          .item-body{
            @include flex-prefix-suffix;
            .prefix{
              flex: 0 0 rem(190);
              .img-wrap{
                position: relative;
                width: 100%;
                height: rem(130);
                border-radius: $gray-box-radius;
                overflow: hidden;
                &.play-btn{
                  &:before{
                    content: '';
                    position: absolute;
                    display: inline-block;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    width: rem(50);
                    height: rem(50);
                    background-image: url(../images/icon_play.png);
                    background-repeat: no-repeat;
                    z-index: 1;
                  }
                }
                img{
                  position: absolute;
                  width: 100%;
                  // height: 100%;
                  top: 50%;
                  transform: translateY(-50%);
                  object-fit: cover;

                }
              }
            }
            .suffix{
              flex-direction: column;
              justify-content: space-between;
              padding-left: rem(20);
              align-items: flex-start;
              flex-basis: 100%;
              .title{
                margin-top: rem(10);
                margin-bottom: rem(5);
                line-height: 1.6;
                font-size: rem(15);
                font-weight: $semi-bold;
                @include ellipsis;
              }
              // .point-tag{
              //   margin-right: rem(10);
              // }
              .item-text, .info{
                color: $fc-gray;
              }
              .item-text{
                flex: 0 0 rem(40);
                height: rem(40);
                width: 100%;
                line-height: rem(20);
                font-size: rem(15);
                @include ellipsis-multi;
                -webkit-line-clamp: 2;
              }
              .info{
                width: 100%;
                @include flex-prefix-suffix;
                margin-top: rem(10);
                flex: 0 1 rem(20);
                font-size: rem(14);
                span{
                  vertical-align: middle;
                  margin-right: rem(5);
                }
                em{
                  margin: 0 rem(3);
                  font-weight: $semi-bold;
                  color: $fc-default;
                }
                .suffix{
                  flex-direction: row;
                }
              }
              
            }
          }
        }
      }
    }
    .today-pick{
      width: calc(#{$container-width} - #{$aside-width} - #{$content-aside-gap} - #{$container-side-pad} * 4);
      padding-bottom: rem(35);
      .header{
        .suffix{
          .btn-question{
            margin-right: rem(5);
          }
        }
      }
      .pick-list{
        padding-top: rem(40);
        >ul{
          display: flex;
          padding-bottom: rem(20);
          >li{
            flex: 0 0 rem(230);
            flex-basis: rem(230);
            width: rem(230);
            height: rem(300);
            margin-right: rem(50);
            &:nth-last-of-type(1){
              padding-right: rem(10);
            }
            >a{
              &.item{
                position: relative;
                .pick-tag{
                  position: absolute;
                  display: inline-flex;
                  justify-content: center;
                  align-items: center;
                  left: 0;
                  top: rem(-26);
                  width: rem(52);
                  height: rem(52);
                  border-radius: 50%;
                  background-color: rgba(255,138,204,0.8);
                  color: #ffffff;
                  font-weight: $bold;
                  font-size: rem(15);
                  z-index: 1;
                  img{
                    width: rem(27);
                  }
                }
                display: block;
                height: 100%;
                .card{
                  display: flex;
                  width: 100%;
                  height: 100%;
                  flex-direction: column;
                  overflow: hidden;
                  border-radius: $gray-box-radius;
                  // box-shadow: $template-shadow;
                  border: 0;
                  .img-wrap{
                    display: inline-block;
                    position: relative;
                    flex: 0 0 rem(150);
                    overflow: hidden;
                    img{
                      position: absolute;
                      top: 50%;
                      transform: translateY(-50%);
                      width: 100%;
                      // height: 100%;
                      object-fit: cover;
                    }
                  }
                  .card-body{
                    display: flex;
                    flex-direction: column;
                    flex: 1 1 rem(150);
                    flex-basis: rem(150);
                    background-color: #ffffff;
                    padding: rem(15);
                    
                    .nickname{
                      font-size: rem(12);
                      color: $fc-gray;
                    }
                    .title{
                      flex-grow: 1;
                      flex-basis: rem(40);
                      max-height: rem(40);
                      @include ellipsis-multi;
                      margin-top: rem(10);
                      font-size: rem(14);
                      font-weight: $semi-bold;
                      color: $fc-default-light;
                      line-height: 1.4;
                    }
                    .item-info{
                      @include flex-prefix-suffix;
                      flex: 1 1 auto;
                      flex-basis: auto;
                      align-items: flex-end;
                      font-size: rem(12);
                      color: $fc-gray;
                      em{
                        color: $fc-default;
                        font-weight: $bold;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    .today-vote{
      margin-top: rem(40);
      .today-vote-list{
        >ul{
          >li{
            .item-wrap{
              position: relative;
              margin-bottom: rem(20);
              > a{
                @include flex-prefix-suffix;
                .avatar{
                  height: rem(100);
                  width: rem(100);
                }
                .prefix{
                  flex: 0 0 rem(100);
                }
                .suffix{
                  display: flex;
                  flex-direction: column;
                  align-items: flex-start;
                  justify-content: flex-start;
                  margin-left: rem(20);
                  padding-top: rem(5);
                  z-index: 1;
                  flex-basis: calc(100% - #{rem(100)});
                  .title{
                    @include ellipsis-multi;
                    // max-width: rem(370);
                    width: 100%;
                    max-height: rem(40);
                    margin-top: rem(10);
                    font-size: rem(14);
                    font-weight: $semi-bold;
                    color: $fc-default-light;
                    line-height: 1.4;
                  }
                  .item-info{
                    flex: 1 1 auto;
                    display: flex;
                    align-items: flex-end;
                    margin-top: rem(10);
                    font-size: rem(12);
                    color: $fc-gray;
                    em{
                      color: $fc-default;
                      font-weight: $bold;
                    }
                  }
                }
              }
            }
            &:nth-last-of-type(1){
              .item-wrap{
                margin-bottom: 0;
              }
            }
          }
        }
      }
    }
    .survey-to-me{
      margin-top: rem(40);
      .survey-to-me-list{
        >ul{
          >li{
            .item-wrap{
              margin-bottom: rem(20);
              a{
                @include flex-prefix-suffix;
                .prefix{
                  flex: 0 0 rem(100);
                  .avatar{
                    height: rem(100);
                    width: rem(100);
                  }
                }
                .suffix{
                  display: flex;
                  flex-direction: column;
                  align-items: flex-start;
                  justify-content: center;
                  margin-left: rem(20);
                  padding-top: rem(5);
                  z-index: 1;
                  .catagory{
                    margin-top: rem(10);
                    color: $fc-gray;
                  }
                  .title{
                    @include ellipsis-multi;
                    margin-top: rem(5);
                    font-size: rem(14);
                    font-weight: $semi-bold;
                    color: $fc-default-light;
                    flex: 1 1 auto;
                  }
                  .item-info{
                    flex: 1 1 auto;
                    display: flex;
                    align-items: flex-end;
                    margin-top: rem(10);
                    font-size: rem(11);
                    color: $fc-gray;
                    em{
                      color: $fc-default;
                      font-weight: $bold;
                    }
                  }
                }
              }
            }
            &:nth-last-of-type(1){
              .item-wrap{
                margin-bottom: 0;
              }
            }
          }
        }
      }
    }
    .row{
      [class*="col-"]{
        flex-grow: 0;
        flex-shrink: 0;
        margin-right: rem(40);
        &:nth-last-of-type(1){
          margin-right: 0;
        }
      }
    }
    .advertisement{
      margin-top: rem(60);
      a{
        display: flex;
        width: 100%;
        img{
          width: 100%;
        }
      }
    }
    .heypoll-mall{
      margin-top: rem(60);
      .heypoll-mall-list{
        >ul{
          display: flex;
          >li{
            flex: 0 1 calc(100% / 4);
            flex-shrink: 1;
            flex-basis: calc(100% / 4);
            margin-right: rem(30);
            &:nth-last-of-type(1){
              margin-right: 0;
            }
            .item-wrap{
              &.card{
                border: 0;
                >a{
                  display: flex;
                  height: 100%;
                  width: 100%;
                  flex-direction: column;
                  .img-wrap{
                    display: inline-block;
                    position: relative;
                    flex: 0 0 rem(150);
                    height: rem(150);
                    overflow: hidden;
                    img{
                      position: absolute;
                      top: 50%;
                      transform: translateY(-50%);
                      width: 100%;
                      // height: 100%;
                      object-fit: cover;
                    }
                  }
                  .card-body{
                    display: flex;
                    flex-direction: column;
                    padding: rem(20) 0;
                    .title{
                      @include ellipsis-multi;
                      height: rem(40);
                      font-size: rem(14);
                      font-weight: $semi-bold;
                      color: $fc-default-light;
                    }
                    .item-info{
                      @include flex-prefix-suffix;
                      margin-top: rem(20);
                      .price{
                        font-size: rem(14);
                        color: $fc-default;
                        font-weight: $bold;
                        .unit{
                          font-size: rem(11);
                          color: $fc-gray;
                          font-weight: $normal;
                        }
                      }
                      .price-to-point{
                        margin-left: rem(5);
                        font-size: rem(14);
                        color: $primary;
                        font-weight: $bold;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    .today-mission{
      margin-top: rem(60);
      .mission-link{
        ul{
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
          li{
            flex: 0 1 calc( 100% / 2 - #{rem(25 / 2)});
            margin-bottom: rem(25);
            a{
              display: block;
              img{
                width: 100%;
              }
            }

          }
        }
      }
    }

    .main-notice{
      margin-top: rem(20);
      padding-bottom: $container-top-pad;
      .title{
        font-size: rem(18);
      }
      .notice-wrap{
        ul{
          li{
            margin-bottom: rem(10);
            &:nth-last-of-type(1){
              margin-bottom: 0;
            }
          }
        }
        .item-wrap{
          position: relative;
          .title{
            @include ellipsis;
            width: rem(370);
            font-size: rem(14);
            font-weight: $semi-bold;
            color: $fc-default-light;
          }
          .item-info{
            flex: 1 1 auto;
            display: flex;
            align-items: flex-end;
            margin-top: rem(8);
            font-size: rem(12);
            color: $fc-gray;
          }
        }
      }
    }
    .news{
      margin-top: rem(20);
      padding-bottom: $container-top-pad;
      .title{
        font-size: rem(18);
      }
      .news-wrap{
        .item-wrap{
          position: relative;
          > a{
            @include flex-prefix-suffix;
            .avatar{
              height: rem(100);
              width: rem(100);
            }
            .prefix{
              flex: 0 0 rem(100);
            }
            .suffix{
              flex-basis: calc(100% - #{rem(100)});
              display: flex;
              flex-direction: column;
              align-items: flex-start;
              justify-content: flex-start;
              margin-left: rem(20);
              // padding-top: rem(5);
              z-index: 1;
              .title{
                @include ellipsis-multi;
                width: 100%;
                max-height: rem(40);
                line-height: 1.4;
                font-size: rem(14);
                font-weight: $semi-bold;
                color: $fc-default-light;
              }
              .item-info{
                flex: 1 1 auto;
                display: flex;
                align-items: flex-end;
                margin-top: rem(10);
                font-size: rem(12);
                color: $fc-gray;
              }
            }
          }
        }
      }
    }
  }
  
}