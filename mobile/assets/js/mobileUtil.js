var console = window.console || {log:function (s){}};

var DatePickerManager = {};

DatePickerManager.init = function(targetId, onSelectFn) {
    $('#' + targetId).datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect : function(dateText) {
            onSelectFn()
        }
    });
}

var FileManager = {};

FileManager.init = function() {
    $('input[type=file]').change(function(){
    	var self = this;
    	var fileName = $(self).attr('ref');
        $('#' + fileName).html($(self).val());
    });
}
FileManager.click = function(targetId) {
    $('#' + targetId).click();
}


var GoToTop = {};

GoToTop.init = function(targetId) {
    var targetObj = $('#' + targetId);

    targetObj.click(function() {
        window.scrollTo(0, 0);
    });
}

GoToTop.position = function(targetId) {
    var windowWidth = $(window).width();
    var width = $('.centeredWithinWrapper').width();

    var targetObj = $('#' + targetId);
    targetObj.css('left', (width + ((windowWidth - width)/2) - 8) + 'px');
    targetObj.css('display', '');
    
}

var LayerPage = {};

LayerPage.init = function(targetClass) {
    var targetObj = $('.' + targetClass);
    
    var height = ($(window).height() - parseInt(targetObj.css('height'))) / 2;
    targetObj.css('margin-top', height - 30 + 'px');
}

LayerPage.position = function(targetClass) {
    var windowHeight = $(window).height();

    var targetObj = $('.' + targetClass);
    var height = parseInt(targetObj.css('height'));
    
    targetObj.css('margin-top', (windowHeight - height) / 2 - 30 + 'px');
}



LoginManager = {};

LoginManager.updateRightHeaderContent = function(ctx) {
    var htmlStr = '';
    if (ctx.notificationCount && ctx.notificationCount > 0) {
    	var count = ctx.notificationCount;
    	if (ctx.notificationCount > 99) {
    	    count = 'N';
    	} 
    	htmlStr += '<a href="/notification"><div class="noti num12"><span>' + count + '</span></div></a>';
    } else {
    	htmlStr += '<a href="/notification"><div class="noti notiNone num12"><span></span></div></a>';
    }
    
    if (ctx.user.profileImage) {
    	var fileUrl = ctx.user.profileImage.fileUrl ;
	var filePath = fileUrl.substring(0, fileUrl.lastIndexOf('/') + 1);
	var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1, fileUrl.length);
	
    	htmlStr += '<div class="profileImage" style="background: url('+ filePath + 'ws' + fileName +')"><a href="/setting"><div></div></a></div>';
    } else {
    	htmlStr += '<div class="profileImage"><a href="/my/point"><div></div></a></div>';
    }
    
    $('.rightHeaderContent').empty();
    $('.rightHeaderContent').append(htmlStr);
};

LoginManager.login = function(success) {
    var self = this;
    Confirm({title:'로그인이 필요한 서비스입니다. T^T', content:'지금 로그인하시고 바로 참여하시겠어요?<br>참 쉬운 회원가입으로 포인트까지 적립해보세요!', buttonTitle: '네!', cancelButtonTitle: '나중에'}, function() {
    	console.log(self)
    	ref = window.location.pathname;
    	window.location.href = '/mobile/login?ref='+ref;
    });   
}


function Alert(opt, onClose) {
    var title = '알림';
    var content = '';
    if (typeof opt === 'object') {
	if (opt.title != undefined) title = opt.title;
	if (opt.content != undefined) content = opt.content;
	if (opt.onClose != undefined) onClose = opt.onClose;
    } else if (typeof opt === 'string') {
	content = opt;
    }
    $("#alert_header").text(title);
    $("#alert_messege").html(content);
    $("#alert_popup").modal({
	  escapeClose: false,
	  clickClose: false,
	  showClose: false
	});
	$("#alert_popup").find('a').unbind("click"); //무조건 unbind를 해주지 않으면 기존 이벤트가 그대로 담아 있다. //19.4.9 yr.seo
    if (onClose != undefined){
    	//$("#alert_popup").find('a').unbind("click"); //unbind를 해주지 않으면 기존 이벤트가 그대로 담아 있다. //19.4.7 yr.seo
    	$("#alert_popup").find('a').bind("click",(function(e) {
    		e.preventDefault();
    		onClose();
    	}));
    };

}

function Confirm(opt, onConfirm, onCancel) {
	
    var title = '잠깐만요!';
    var content = '';
    var buttonTitle = '확인';
    var cancelButtonTitle = '취소';    

    if (typeof opt === 'object') {
	if (opt.title != undefined) title = opt.title;
	if (opt.content != undefined) content = opt.content;
	if (opt.buttonTitle != undefined) buttonTitle = opt.buttonTitle;
	if (opt.cancelButtonTitle != undefined) cancelButtonTitle = opt.cancelButtonTitle;
	if (opt.onConfirm != undefined) onConfirm = opt.onConfirm;
	if (opt.onCancel != undefined) onCancel = opt.onCancel;	
    } else if (typeof opt === 'string') {
	content = opt;
    }

    $("#confirm_header").text(title);
    $("#confirm_messege").html(content);
    $("#confirm_confirm").text(buttonTitle); //확인 버튼 텍스트 변경 추가 19.3.15 yrseo
    $("#confirm_cancel").text(cancelButtonTitle); //취소 버튼 텍스트 변경 추가 19.3.15 yrseo
    $("#confirm_popup").modal({
	  escapeClose: false,
	  clickClose: false,
	  showClose: false
	});
    if (onConfirm != undefined){
    	$("#confirm_popup").find('#confirm_confirm').unbind("click");
    	$("#confirm_popup").find('#confirm_confirm').bind("click",(function(e) {
    		e.preventDefault();
    		onConfirm.call(this);
    		//modal close
    		$("#confirm_popup .btn_close").click();
    		
    	}));
    	//);
    };
    if (onCancel != undefined){
    	$("#confirm_popup").find('#confirm_cancel').unbind("click");
    	$("#confirm_popup").find('#confirm_cancel').bind("click",onCancel);
    };
}

//맛보기 설문 동의창 팝업
function MobileIntroConfirm(opt, onClose, onCancel) {
    $("#introPopup").modal({
	  escapeClose: false,
	  clickClose: false,
	  showClose: false,
	});
    $("#introPopup").css("border-radius","0px");
    $("#introPopup").find("button").click(function(e) {
		e.preventDefault();
		onClose.call(this);
		//modal close
		$("#introPopup .btn_close").click();
	});
}


// Scrollbar reaches edge event detector
ScrollEdgeDetector = {
    option: {
	onTopEdge: function() {},
	onBottomEdge: function() {},
	topEdgeStayTime : 0,
	bottomEdgeStayTime: 0,
	topEdgeDistance: 0,
	bottomEdgeDistance: 0
    },
    setOption: function(opt) {
	var self = this;
	var prop;
	for (prop in opt) {
	    self.option[prop] = opt[prop];
	}
    },
    isBottomEdge: function() {
	var self = this;

	if (self.jqDoc.height() > self.jqWin.height() && 
	    self.jqDoc.scrollTop() + self.jqWin.height() + self.option.bottomEdgeDistance >= self.jqDoc.height()) {
	    return true;
	} 
	return false;
    },
    isTopEdge: function() {
	var self = this;
	if (self.jqDoc.scrollTop() <= self.option.topEdgeDistance) {
	    return true;
	}
	return false;
    },
    init: function(opt) {
	var self = this;
	self.setOption(opt);
	
	self.jqWin = $(window);
	self.jqDoc = $(document);
	
	var scrollListener = function(e) {
	    if (self.option.onBottomEdge && self.isBottomEdge()) {

		if (typeof self.option.onBottomEdge === 'function') {
		    if (self.bottomTimeout == undefined) {
			self.bottomTimeout = setTimeout(function() {
			    self.bottomTimeout = undefined;
			    if (self.isBottomEdge()) {				
				self.option.onBottomEdge();			
			    }
			}, self.option.bottomEdgeStayTime < 10 ? 10 : self.option.bottomEdgeStayTime);
		    }
		}
	    }
	    else if (self.option.onTopEdge && self.isTopEdge()) {
		if (typeof self.option.onTopEdge === 'function') {
		    if (self.topTimeout == undefined) {
			self.topTimeout = setTimeout(function() {
			    self.topTimeout = undefined;
			    if (self.isTopEdge()) {				
				self.option.onTopEdge();
			    }
			}, self.option.topEdgeStayTime < 10 ? 10 : self.option.topEdgeStayTime);
		    }
		}
	    }
	};

	self.jqWin.bind('scroll', scrollListener);
    }
}


// jQuery Transit 플러그인에서 사용하는 CSS3 Transform api가 지원되지 않으면
// 기존 jQuery animate를 사용하도록 한다
if (!$.support.transition) {
    $.fn.transition = $.fn.animate;
}

// IE trim() patch
if(typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, ''); 
    }
}



function number_format(num){
    if (!num) return num;

    var num_str = num.toString().replace(/,/gi, "");
    var result = "";
    
    for(var i=0; i<num_str.length; i++){
        var tmp = num_str.length - (i+1);
	
        if(((i%3)==0) && (i!=0))    result = ',' + result;
        result = num_str.charAt(tmp) + result;
    }
    return result;
}


// isTablet()과 isMobile()은 HP와 SRP에서 사용 중
function isMobileDevice() {
    var phoneArray = new Array('samsung-', 'sch-', 'shw-', 'sph-', 'sgh-', 'lg-', 'canu', 'im-', 'ev-', 'iphone', 'nokia', 'blackberry', 'lgtelecom', 'natebrowser', 'sonyericsson', 'mobile', 'android', 'ipad');
    for (i = 0; i < phoneArray.length; i++) {
	if (navigator.userAgent.toLowerCase().indexOf(phoneArray[i]) > -1) {
	    return true;
	    }
	}
    return false;
}
function isTabletDevice() {
    if (!isMobileDevice()) {
	return false;
	}
    // 태블릿검사
    if (navigator.userAgent.toLowerCase().indexOf('ipad') > -1 ||
	(navigator.userAgent.toLowerCase().indexOf('android') > -1 && navigator.userAgent.toLowerCase().indexOf('mobile') == -1)) {
	return true;
	}
    // 갤럭시 탭만을 위한 리다이렉트. Mobile 이라는 단어가 안들어오게 되면 지우셔도 됨
    var galaxyTabModel = new Array('shw-');
    for (i = 0; i < galaxyTabModel.length; i++) {
	if (navigator.userAgent.toLowerCase().indexOf(galaxyTabModel[i]) > -1) {
	    return true;
	    }
	}
    return false;
}

// yrseo 19.1.11 투표등록하기 추가(투표등록화면 크로스부라우징 처리)
var browser;
jQuery.uaMatch = function (ua) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
        /(webkit)[ \/]([\w.]+)/.exec(ua) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];

    return {
        browser: match[1] || "",
        version: match[2] || "0"
    };
};
// Don't clobber any existing jQuery.browser in case it's different
if (!jQuery.browser) {
    matched = jQuery.uaMatch(navigator.userAgent);
    browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }

    jQuery.browser = browser;
}

//fix placeholder for ie7,8
function fixPlaceholder(parentView) {
 if($.browser.msie){ 
	if (parentView == undefined) {
	    parentView = 'body';
	}
	
	$(parentView).find('input[placeholder]').each(function(index, elem){  
         var input = $(elem);                    
	    if (input.val() == '') {
		input.val(input.attr('placeholder'));
	    }
         
	    if (!input.data('placeholder_focus_bind')) {
		input.focus(function(){
		    if (input.val() == input.attr('placeholder')) {
			input.val('');
		    }
		});
		input.data('placeholder_focus_bind', true);
	    }
	    if (!input.data('placeholder_blur_bind')) {
		input.blur(function(){
		    if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.val(input.attr('placeholder'));
		    }
		});
		input.data('placeholder_blur_bind', true);
	    }	    	    
	});
 };
}
window.fixPlaceholder = fixPlaceholder;
$(function() {
 fixPlaceholder();
});
