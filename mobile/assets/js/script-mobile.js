//아코디언
$('document').ready(function(){
  $('.accordion-list > ul > li').click(function(e){
    $(this).addClass('on');
    $(this).find('.detail').slideToggle();
  });
  scrollControl();
  allMenu();
  quickMenu();
  


});
// gnb
function scrollControl(){

  var lastPosition = 0;
  $(window).scroll(function(e){
    // now
    var scrollTop = $(this).scrollTop();
    var scrollDelay = 50;
    var iPhoneUI_gap = 20;

    var scrolled = scrollTop - lastPosition;

    if( scrolled < 0 && scrolled < -iPhoneUI_gap ){
      // scroll upside
      $('body > .gnb').removeClass('scroll-down').addClass('scroll-up');

    } else if( scrolled > 0 && scrolled > iPhoneUI_gap) {
      // scroll downside
      $('body > .gnb').removeClass('scroll-up');
    }

    if( scrollTop > 30 ){
      $('.gnb.sticky').removeClass('shadow');
      $('.gnb.top').addClass('scroll-down');
    } else {
      $('.gnb.sticky').addClass('shadow');
    }

    lastPosition = scrollTop;
  });
}


// 전체메뉴관련
function allMenu(event){
  var scrollY = 0;
  $('.btn-all-menu').click(function(e){
    e.preventDefault();
    scrollY = $(document).scrollTop();
    $('.all-menu').addClass('active');
    $('body').css('overflow', 'hidden');
    $('body').css('position', 'fixed');
  });

  $('.btn-all-menu-close').click(function(e){
    e.preventDefault();
    $('.all-menu').removeClass('active');
    $('body').css('overflow', 'visible');
    $('body').css('position', 'relative');
    $(document).scrollTop(scrollY);
  });
}
function mainBannerCounter(event){
  var element   = event.target;         // DOM element, in this example .owl-carousel
  var items     = event.item.count;     // Number of items
  var item      = event.item.index + 1;     // Position of the current item

  // it loop is true then reset counter from 1
  if(item > items) {
    item = item - items
  }
  $('#mainBannerCounter').html("<span class='now'>"+item+"</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class='total'>"+items+"</span>")
}
function todayPlayCounter(event){
  var element   = event.target;         // DOM element, in this example .owl-carousel
  var items     = event.item.count;     // Number of items
  var item      = event.item.index + 1;     // Position of the current item

  // it loop is true then reset counter from 1
  if(item > items) {
    item = item - items
  }
  $('#todayPlayCounter').html("<span class='now'>"+item+"</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class='total'>"+items+"</span>")
}
//퀵메뉴
function quickMenu(event){
  $('.quick-menu .menu-list button').click(function(){
    if( $(this).hasClass('toggle') ){
      $('.quick-menu').toggleClass('active');
      $(this).toggleClass('on');
    }
  })
}
